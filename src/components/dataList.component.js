import { Button } from 'react-bootstrap'

function DataList (props) {
    return (
        <section id="moviesList">
            <div className="container">
                <div className="row">
                    <div className="col-12 text-left">
                        <ul className="arrow-styled">
                            {
                                props.movies.map((movie, index) => (
                                    <div key={index} className="row">
                                        <div className="col-md-6 col-sm-12">
                                            <h3>{movie.Title}</h3>
                                        </div>
                                        <div className="col-md-6 col-sm-12">
                                            <Button variant="danger" size="sm" onClick={() => props.onAdd(movie)}>
                                                <li>Agregar</li>
                                            </Button>
                                        </div>
                                    </div>
                                ))
                            }
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default DataList